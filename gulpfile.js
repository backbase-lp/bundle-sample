'use strict';
var gulp = require('gulp');
var g = require('gulp-load-plugins')();
var path = require('path');

var browserSync = require('browser-sync');
var packageJson = require('./package.json');


var paths = {
	resources : './resources',
	widgets : './widgets'
};

gulp.task('browser-sync', function() {
	browserSync({
		server: {
			baseDir: './'
		},
		open: false
	});
});

gulp.task('bs-reload', function () {
	browserSync.reload();
});

/*----------------------------------------------------------------*/
/* Watcher
/*----------------------------------------------------------------*/
gulp.task('start', ['browser-sync'], function () {
	gulp.watch([
			paths.resources + '/**/*.js',
			'!' + paths.resources + '/**/node_modules/**'
	], ['bs-reload']);
	console.log(paths.widgets);
	gulp.watch([
			paths.widgets + '/**/*.js',
			'!' + paths.widgets + '/**/bower_components/**',
			'!' + paths.widgets + '/**/node_modules/**'
	], ['bs-reload']);


	gulp.watch('*.html', ['bs-reload']);
});


/*----------------------------------------------------------------*/
/* Installer TODO
/*----------------------------------------------------------------*/
// gulp.task('install:resources', function () {
// 	var bower = require('bower');
// 	bower.config = {
// 		// installed
// 	};

// 	var resourcesPaths = Object.keys(packageJson.resources);
// 	resourcesPaths.forEach(function(i){
// 		var success = function(installed) {

// 		};
// 		(function(cli) {
// 			var dir = paths.resources;
// 			var url = packageJson.resources[i];
// 			if(url.indexOf('git') < 0) { url = i; } // remove when we have a bower registered repo
// 			cli.install([ url ], {save: true} , { directory: dir })
// 			.on('end',  success);

// 		})(bower.commands);
// 	});
// });


// gulp.task('install', ['install:resources','install:widgets'], function () {

// });
